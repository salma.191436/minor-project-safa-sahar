"""
URL configuration for backend project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from osm.views import LocationCreateView,LocationDataView
from cform.views import ContributionCreateView,ContributionDataView

from rest_framework_simplejwt.views import(
     TokenObtainPairView, 
     TokenRefreshView,
)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/user/', include('userman.urls')),
     path('api/locations/', LocationCreateView.as_view(), name='location-create'),
    path('api/get_location_data/', LocationDataView.as_view(), name='get_location_data'),
      path('api/contribution/', ContributionCreateView.as_view(), name='contribution-create'),
    path('api/get_contribution_data/', ContributionDataView.as_view(), name='get_contribution_data'),
     path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
]
