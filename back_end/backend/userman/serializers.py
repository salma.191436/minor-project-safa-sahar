from rest_framework import serializers
from userman.models import CustomUser



class RegisterSerializer(serializers.ModelSerializer):
    userType = serializers.CharField(write_only=True, required=False)
    class Meta:
        model = CustomUser
        fields = ['name', 'address', 'phoneNumber', 'email', 'password', 'userType']
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
            userType = validated_data.pop('userType', None)
            user = CustomUser(
            name = validated_data['name'],
            address = validated_data['address'],
            phoneNumber = validated_data['phoneNumber'],
            email = validated_data['email'],
            userType = userType
            )
           
            user.set_password(validated_data['password'])
            user.save()
            return user
    


class LoginSerializer(serializers.Serializer):
     email = serializers.EmailField()
     password = serializers.CharField(write_only=True)


    
