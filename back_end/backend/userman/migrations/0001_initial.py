# Generated by Django 4.2.5 on 2023-09-28 04:52

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='CustomUser',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('address', models.CharField(max_length=200)),
                ('email', models.EmailField(max_length=254, unique=True)),
                ('phoneNumber', models.CharField(max_length=10)),
                ('password', models.CharField(max_length=128)),
                ('userType', models.CharField(choices=[('N', 'Nagarik'), ('G', 'G-collector'), ('S', 'SafaGhar')], max_length=1)),
            ],
        ),
    ]
