from django.db import models

# Create your models here.
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.contrib.auth.hashers import make_password, check_password


class CustomUser(models.Model):
    USER_TYPE_CHOICES = (
        ('N', 'Nagarik'),
        ('G', 'G-collector'),
        ('S', 'SafaGhar'),
    )
    name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    email = models.EmailField(unique=True)
    phoneNumber = models.CharField(max_length=10)
    password = models.CharField(max_length=128)
    userType = models.CharField(max_length=1, choices=USER_TYPE_CHOICES)

    

    
    def __str__(self):
        return self.email
    
    def set_password(self, raw_password):
        self.password = make_password(raw_password)

    def check_password(self, raw_password):
        return check_password(raw_password, self.password)
    
