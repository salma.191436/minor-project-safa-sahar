from django.db import models

class Location(models.Model):
     locationName = models.CharField(max_length=200, blank=True)
     latitude = models.DecimalField(max_digits=25, decimal_places=15)
     longitude = models.DecimalField(max_digits=25, decimal_places=15)
def __str__(self):
        return f"{self.locationName} (Lat: {self.latitude}, Lon: {self.longitude})"

