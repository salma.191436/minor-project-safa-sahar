# Generated by Django 4.2.5 on 2023-09-28 05:15

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Location',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('locationName', models.CharField(blank=True, max_length=200)),
                ('latitude', models.DecimalField(decimal_places=7, max_digits=25)),
                ('longitude', models.DecimalField(decimal_places=7, max_digits=25)),
            ],
        ),
    ]
