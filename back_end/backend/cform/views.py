from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from .serializers import ContributionSerializer
from .models import Contribution
from django.http import JsonResponse
from django.views import View
import json

class ContributionCreateView(APIView):
    def post(self, request):
        # Get the JSON data from the request
        jsonData = request.body.decode('utf-8')
        
        try:
            # Parse the JSON data
            data = json.loads(jsonData)
        except json.JSONDecodeError:
            return Response({"error": "Invalid JSON data"}, status=status.HTTP_400_BAD_REQUEST)

        # Validate the incoming data
        serializer = ContributionSerializer(data=data)

        if serializer.is_valid():
            # Create a new instance of the Location model and save it
            location = Contribution(**serializer.validated_data)
            location.save()
            
            # Return a success response
            return Response({"message": "Contribution value saved successfully"}, status=status.HTTP_201_CREATED)
        else:
            # Return a validation error response
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

 
class ContributionDataView(View):
    def get(self, request):
        latest_location = Contribution.objects.latest('id')

        serializer = ContributionSerializer(latest_location)
        
        return JsonResponse(serializer.data)

 