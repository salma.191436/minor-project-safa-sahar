from django.db import models

class Contribution(models.Model):
    wasteType = models.CharField(max_length=100)
    totalWaste = models.IntegerField(default=0)
    totalAmount = models.IntegerField()
    totalDonation = models.IntegerField()

    def __str__(self):
        return f"{self.wasteType} (Total Waste: {self.totalWaste}, Total Amount: {self.totalAmount}, Total Donation: {self.totalDonation})"
