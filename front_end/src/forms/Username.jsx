
import React, { useState } from 'react';
import axios from 'axios';
import './username.css';
import Footer from '../components/Footer';
const Username = () => {
  const [username, setUsername] = useState('');
  const [changeUsername, setChangeUsername] = useState('');
  const [confirmUsername, setConfirmUsername] = useState('');

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      const response = await axios.post('http://', {
        username,
        changeUsername,
        confirmUsername,
      });

      
      console.log(response.data);
    } catch (error) {
      
      console.error(error);
    }
  };

  return (
    <>
      <form onSubmit={handleSubmit} className='usernameform'>
        <span className='usernamespan'>
          Current UserName: <input type="text" name='username' value={username} onChange={(e) => setUsername(e.target.value)}  className='usernameinput'/>
        </span>
        <span className='usernamespan'>
          Change UserName: <input type="text" name='changeusername' value={changeUsername} onChange={(e) => setChangeUsername(e.target.value)} className='usernameinput'/>
        </span>
        <span className='usernamespan'>
          Confirm Username: <input type="text" name='confirmusername' value={confirmUsername} onChange={(e) => setConfirmUsername(e.target.value)} className='usernameinput'/>
        </span>
        <button type="submit" className='usernamebutton'>Submit</button>
      </form>
      <Footer />
      </>
  );
};

export default Username;
