
import React, { useState } from 'react';
import axios from 'axios';
import './password.css';
import Footer from '../components/Footer';

const Password = () => {
  const [currentPassword, setCurrentPassword] = useState('');
  const [changePassword, setChangePassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
  
      const response = await axios.post('http://', {
        currentPassword,
        changePassword,
        confirmPassword,
      });

      console.log(response.data);
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <>
      <form onSubmit={handleSubmit} className='passwordform'>
        <span className='passwordspan'>
          Current Password:<input type="password" name='password' value={currentPassword} onChange={(e) => setCurrentPassword(e.target.value)}  className='passwordinput'/>
        </span>
        <span className='passwordspan'>
          Change Password:<input type="password" name='changepassword' value={changePassword} onChange={(e) => setChangePassword(e.target.value)} className='passwordinput'  />
        </span>
        <span className='passwordspan'>
          Confirm Password:<input type="password" name='confirmpassword' value={confirmPassword} onChange={(e) => setConfirmPassword(e.target.value)} className='passwordinput' />
        </span>
        <button type="submit" className='passwordbutton'>Submit</button>
      </form>
      <Footer />
    </>
  );
};

export default Password;

