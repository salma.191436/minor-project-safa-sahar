
import React, { useEffect, useState } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import axios from 'axios'; 
import './locationcomponent.css'

function LocationComponent() {
  const [locationData, setLocationData] = useState(null);

  useEffect(() => {
    const fetchDataAndNotify = async () => {
      try {
        const response = await axios.get('http://127.0.0.1:8000/api/get_location_data/'); 
        console.log('API Response:', response.data);
        if (response.data) {
          const { locationName, latitude, longitude } = response.data;

          toast.info(`Location: ${locationName}, Latitude: ${latitude}, Longitude: ${longitude}`);
          setLocationData(response.data);
        }
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };

    fetchDataAndNotify();
  }, []);

  return (
    <div>
      <ToastContainer/>
    </div>
  );
}

export default LocationComponent;
