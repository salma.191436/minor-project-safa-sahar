import './nagnav.css'
import React, { useState } from 'react'
import {Link, NavLink} from 'react-router-dom'
import newlogo from '../images/newlogo.png'

import {BsPersonCircle} from 'react-icons/bs'
import {Naglink} from '../data'
import {Main} from '../data'


const NagNav = () => {

const [show,setShow]=useState(false);

function handleShow(){
  setShow(!show);
}

  return (
    <div className='ngnav'>
        <div className='ngnav__content'>
            <img src={newlogo} className='nglogo'/>
            <ul className='ngnav__links'>
              {
                Main.map(({name,path},index)=>{
                return(
                  <li key={index}>
                    <NavLink to={path} className='ngnav__link'>{name}</NavLink>
                  </li>
                )
                })
              }
            </ul>
        <div className='ngsafaghar__btn' onClick={handleShow}>
        <BsPersonCircle/>
        <div className='ngshowbox'>
        {
          show && <ul> {
            Naglink.map(({name,path},index)=>{
              return(
                <li className='ngshowlist' key={index}>
                  <Link to={path} className='ngtext__container'>{name}</Link></li>
              )

            })
          }</ul>
        }
        </div>
    </div>
            
        </div>

        
    </div>
  )
}

export default NagNav
