import React from 'react'
import { Outlet } from 'react-router-dom'
import GheroNav from './GheroNav'

const GheroLayout = () => {
  return (
    <div>
    <header>
    <GheroNav />
</header>
<main><Outlet />
</main>
</div>

  )
}

export default GheroLayout
