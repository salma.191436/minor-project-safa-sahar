import React from 'react'
import { Outlet } from 'react-router-dom'
import NagNav from './NagNav'

const NagarikLayout = () => {
  return (
    <div>
    <header>
    <NagNav />
</header>
<main><Outlet />
</main>
</div>

  )
}

export default NagarikLayout
