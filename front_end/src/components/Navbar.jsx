import { Link, NavLink} from 'react-router-dom'
import './navbar.css'
import Logo from '../images/newlogo.png'
import { links} from '../data'

const Navbar = () => {
  return (
    <div className='mnav'>
      <Link to="/" className='mlogo'>
        <img src={Logo} alt="Nav logo"/>
      </Link>
<ul className='mnav__links'>
    {
        links.map(({name, path}, index) =>{
            return(
                <li key={index}>
                    <NavLink to ={path} className='msafasahar_link'> {name}  </NavLink>
                </li>
            )
        })
    }
</ul>   
    </div>
  )
}


export default Navbar;
