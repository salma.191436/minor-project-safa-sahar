import './gheronav.css'
import React, { useState } from 'react'
import {Link} from 'react-router-dom'
import newlogo from '../images/newlogo.png'

import {BsPersonCircle} from 'react-icons/bs'
import {Naglink} from '../data'

const GheroNav = () => {

const [show,setShow]=useState(false);

function handleShow(){
  setShow(!show);
}

  return (
    <>
      <div className="gheronav">
        <div className='gheronav__content'>
            <img src={newlogo} className='gherologo'/>
        <div className='gherobtn' onClick={handleShow}>
        <BsPersonCircle/>
        <div className='gheroshowbox'>
        {
          show && <ul> {
            Naglink.map(({name,path},index)=>{
              return(
                <li className='gheroshowlist' key={index}>
                  <Link to={path} className='gherotext__container'>{name}</Link></li>
              )

            })
          }</ul>
        }
        </div>
    </div>
            
        </div>
      </div>
        
    </>
  )
}

export default GheroNav
