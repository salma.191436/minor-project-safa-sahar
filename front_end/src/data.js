
const links =[
    {
        name:"Home",
        path:'/'
     
    },
    {
        name:"About",
        path:'/about'
     
    },
    {
        name:"3Rwaste",
        path:'/3Rwaste'
     
    },
    {
        name:"GHero",
        path:'/GHero'
     
    },
    {
        name:"Login",
        path:'/login'
     
    },
    {
        name:"Register",
        path:'/register'
     
    },
    
    ]
    const gcollector=[
        {
            photo:"gcollector1.jpg",
            gid:1,
            name:"Jeevan  Chand",
            address:"Chitwan"
            
        },
        {
            photo:"gcollector2.jpg",
            gid:2,
            name:"Laxman Bola",
            address:"Salyan"
            
    
        },
        {
            gid:3,
            name:"Shreya Sahani",
            address:"Rukum",
            photo:"gcollector4.jpg"
        },
        {
            gid:4,
            name:"Shyam Karki",
            address:"Dolpa",
            photo:"gcollector5.jpg"
        },
        {
            gid:5,
            name:"Srijana Sahani ",
            address:"Bharatpur",
            photo:"pp3.jpg"
        }
    ]
    const wastetype=[
        {
            name:"Paper",
            path:'/paper'
        },
        {
            name:"E-waste",
            path:'/ewaste'
        },
        {
            name:"Bottle",
            path:'/bottle'
        },
        {
            name:"Metal&Steel",
            path:'/metalsteel'
        },
    
    ]
    const waste=[
        {
            Type:"Paper waste",
            name:"Books",
            Cost :25,
        },
        {
            Type:"Paper waste",
            name:"Copy",
            Cost :25
        },
        {
            Type:"Paper waste",
            name:"Newspaper",
            Cost:25
        },
        {
            Type:"Paper waste",
            name:"Magazine",
            Cost :25
        },
        {
            Type:"Glass Waste",
            name:"Beer Bottles",
            Cost :40
        },
        {
            Type:"Plastic waste",
            name:"Plastic bottles",
            Cost :30
        },
        {
            Type:"Jute waste",
            name:"Jute bags",
            Cost :45
        },
        {
            Type:"Jute waste",
            name:"jute reusable items",
            Cost :45
        },
        {
            Type:"Metal Waste",
            name:"copperitems",
            Cost :300}
            ,
        {
            Type:"Metal Waste",
            name:"steelitems",
            Cost :300}
            ,
            {
            Type:"E-waste",
            name:"computer",
            Cost:250}
            ,
            {
            Type:"E-waste",
            name:"generator",
            Cost :250}
            ,
            {
            Type:"E-waste",
            name:"battery",
            Cost :250}
    
        ]
        const Naglink = [
        
            {
              name: "Change Username",
              path: 'changeusername'
            },
            {
              name: "Change Password",
              path: 'changepassword'
            },
            
            {
              name: "Logout",
              path: '/'
            }
          ]
         const Main =[
            {
              name: "Home",
              path: 'naghome'
            },
            {
              name: "OpenStreetMap",
              path: 'osm'
            },
            {
              name: "ContributionForm",
              path: 'cform'
            }
          ]
    
    
    export  {links,gcollector,waste,wastetype, Naglink , Main};
    
        
    