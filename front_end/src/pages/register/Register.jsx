
import './register.css'
import React, { useState } from 'react';
import Footer from '../../components/Footer';
import { useNavigate} from 'react-router-dom'; 

const Register = () => {
   const navigate=useNavigate();
  const [name, setName] = useState('');
  const [address, setAddress] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [userType, setUserType] = useState('nagarik');

  const handleSubmit = (e) =>{
    e.preventDefault();

    const trimmedName = name.trim();
    if (!trimmedName) {
      alert('Please enter a valid name.');
      return;
    }

    if (!/^[a-zA-Z]+$/.test(trimmedName)) {
      alert('Name must contain only letters.');
      return;
    }
    if (!/^[a-zA-Z\s]+$/.test(address)) {
      alert('Address must contain only letters and spaces.');
      return;
    }
    if (!/^\d{10,}$/.test(phoneNumber)) {
      alert('Phone number must be a valid 10-digit number.');
      return;
    }
    
    if (!/^\S+@\S+\.\S+$/.test(email)) {
      alert('Please enter a valid email address.');
      return;
    }
    let  register_data = {
      name: trimmedName, 
      address,
      phoneNumber,
      email,
      password,
      confirmPassword,
      userType,
    };
    console.log(register_data);
    console.log(JSON.stringify(register_data))
    fetch('http://127.0.0.1:8000/api/user/api/register/', {
      method: 'POST',
      headers: { 'content-type': 'application/json' },
      body: JSON.stringify(register_data),
     
    })
      .then((res) => {
        alert('Registered Successfully');
        navigate('/login');
      })
      .catch((err) => {
        alert('Failed: ' + err.message);
      });
  } 
  return (
    <>
        <form onSubmit={handleSubmit} className='registerform'>
          <h2 className='registerh2'>Register</h2>
          <div>
            <label className='registerlabel'>Name</label>
            <input type="text"  value={name} onChange={e=>setName(e.target.value)} autoComplete="off" className='registerinput'  />
          </div>

          <div>
          <label className='registerlabel'>Address</label>
            <input type="text" name="address"  value={address}  onChange={e=>setAddress(e.target.value)}  autoComplete="off" className='registerinput'/>
          </div>

          <div>
          <label className='registerlabel'>Number</label>
            <input  type="text"   value={phoneNumber} onChange={e=>setPhoneNumber(e.target.value)} autoComplete="off" className='registerinput'/>
          </div>
          <div >
          <label className='registerlabel'>Email</label>
            <input type="text"  value={email} onChange={e=>setEmail(e.target.value)}  autoComplete="off" className='registerinput'/>
          </div>

          <div >
          <label className='registerlabel'>Password</label>
            <input type="password"  value={password} onChange={e=>setPassword(e.target.value)}  autoComplete="off"className='registerinput'/>
          </div>
          <div >
          <label className='registerlabel'>Confirm password</label>
            <input  type="password"  value={confirmPassword}  onChange={e=>setConfirmPassword(e.target.value)}  autoComplete="off"className='registerinput'/>
          </div>
          
          <div>
            <input  type="radio" checked={userType==='nagarik'} onChange={()=>setUserType('nagarik')} id="nagarik" name="userType"className='registerradio' />
            <label htmlFor="nagarik"className='radiolabel'>Nagarik</label>

            <input  type="radio" checked={userType==='g_collector'} onChange={()=>setUserType('g_collector')} id="g_collector" name="userType" className='registerradio' />
            <label htmlFor="g_collector" className='radiolabel'>G-collector</label>

            <input  type="radio" checked={userType==='safa_ghar'} onChange={()=>setUserType('safa_ghar')} name="userType" id="safa_ghar" className='registerradio'/>
            <label htmlFor="safa_ghar" className='radiolabel'>Safa Ghar</label>
          </div>
          <button type="submit" className="registerbutton">Register</button>
        </form>
      <Footer/>
    </>
  );
};

export default Register;

