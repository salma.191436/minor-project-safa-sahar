
import React, { useState, useRef, useEffect, useContext } from 'react';
import AuthContext from '../../context/AuthProvider';
import axios from '../../api/axios';
import { Link } from 'react-router-dom';
import Footer from '../../components/Footer';
import './login.css';
import {useNavigate} from 'react-router-dom';

const LOGIN_URL = 'http://127.0.0.1:8000/api/user/login/'; 

const Login = () => {
  const { setAuth } = useContext(AuthContext);
  const emailRef = useRef();
  const errRef = useRef();
  const navigate=useNavigate();
  const [email, setEmail] = useState(''); 
  const [password, setPassword] = useState('');
  const [errMsg, setErrMsg] = useState('');
  const [success, setSuccess] = useState(false);

  useEffect(() => {
    emailRef.current.focus();
  }, []);

  useEffect(() => {
    setErrMsg('');
  }, [email, password]);

  const handleSubmit = async (e) => {
    e.preventDefault();
    console.log(email, password);
    try {
      const response = await axios.post(
        LOGIN_URL,
        JSON.stringify({ email, password }), 
        {
          headers: { 'Content-Type': 'application/json' },
          withCredentials: true,
        }
      );
      console.log(JSON.stringify(response?.data));
      //console.log(JSON.stringify(response));
      const accessToken = response?.data?.accessToken; 
      const user_type = response?.data?.user_type;
      console.log(user_type);

      if (user_type === 'nagarik') {
        console.log('Navigating to /nagarik/naghome');
        navigate('/nagarik/naghome'); 
      } else if (user_type === 'g_collector') {
        console.log('Navigating to /ghero/ghome');
        navigate('/ghero/ghome'); 
      }
    } catch (err) {
      if (!err?.response) {
        setErrMsg('No Server Response');
      } else if (err.response?.status === 400) {
        setErrMsg('Missing Email or password'); 
      } else if (err.response?.status === 401) {
        setErrMsg('unauthorized');
      } else {
        setErrMsg('Login Failed');
      }

    }
  };

  return (
    <>
      {success ? (
        <section>
          {' '}
          <h1>you are logged in</h1>
          <br />
        </section>
      ) : (
        <section>
          <form onSubmit={handleSubmit} className="loginform">
            <h2 className="loginh2">Login</h2>
            <div>
              <label className="loginlabel">Email</label> 
              <input  type="text"  name="email"  ref={emailRef}  value={email}  required  onChange={(e) => setEmail(e.target.value)}  autoComplete="off"   className="logininput"  />
            </div>
            <div>
              <label className="loginlabel">Password</label>
              <input  type="password"  name="password" value={password} required onChange={(e) => setPassword(e.target.value)}  className="logininput"  />
            </div>
            <button type="submit" className="loginbutton">
              Login
            </button>
            <div className="loginlinks">
              <Link to="/register" className="abc">
                Register Now
              </Link>
            </div>
          </form>
          <Footer />
        </section>
      )}
    </>
  );
};

export default Login;
