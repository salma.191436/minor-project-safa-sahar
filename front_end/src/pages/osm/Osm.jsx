import React, { useState, useEffect } from "react";
import axios from "axios";
import L from 'leaflet/dist/leaflet';
import icon from "leaflet/dist/images/marker-icon.png";
import iconShadow from "leaflet/dist/images/marker-shadow.png";
import'./osm.css';
import Footer from "../../components/Footer";
import 'leaflet/dist/leaflet.css';
function Osm(props) {
  let DefaultIcon = L.icon({ iconUrl: icon, shadowUrl: iconShadow, });
  const [latitude, setLatitude] = useState(null);
  const [longitude, setLongitude] = useState(null);
  const [locationName, setLocationName] = useState("");

  useEffect(() => {
    const getLocation = () => {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          setLatitude(position.coords.latitude);
          console.log(position.coords.latitude);
          console.log(position.coords.longitude);
          setLongitude(position.coords.longitude);
        },
        (error) => {
          console.warn("Error Code" + error.code + ": " + error.message);
          alert("Check the console");
        }
      );
    };

    getLocation();
  }, []);

  useEffect(() => {
    if (latitude !== null && longitude !== null) {
      const getLocationName = () => {
        axios.get(`https://nominatim.openstreetmap.org/reverse?format=jsonv2&lat=${latitude}&lon=${longitude}`)
          .then((response) => {
            const placeName = response.data.display_name;
            const parts = placeName.split(',');
            const extractedLocationName = parts[0].trim(); // Extract and trim the location name
            console.log(extractedLocationName);
            setLocationName(extractedLocationName);
          })
          .catch((error) => {
            console.error("Error fetching location name:", error);
          });
      };

      const container = L.DomUtil.get("myMap");
      if (container != null) { container._leaflet_id = null; }

      const map = L.map("myMap").setView([latitude, longitude], 12);

      L.tileLayer("https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}", {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 20,
        id: "mapbox/streets-v11",
        tileSize: 512,
        zoomOffset: -1,
        accessToken: "pk.eyJ1IjoidGFyLWhlbCIsImEiOiJjbDJnYWRieGMwMTlrM2luenIzMzZwbGJ2In0.RQRMAJqClc4qoNwROT8Umg",
      }).addTo(map);
      L.Marker.prototype.options.icon = DefaultIcon;

      if (typeof props.lan !== 'undefined' && typeof props.lon !== 'undefined'){
        const marker = L.marker([props.lan, props.lon]).addTo(map);
        marker.bindPopup("<b>destination</b>").openPopup();
      }

      const marker1 = L.marker([latitude, longitude]).addTo(map);
      marker1.bindPopup("<b>Your location</b>").openPopup();

      getLocationName();
    }
  }, [latitude, longitude, props.lan, props.lon]);

  // Move the sendLocationToBackend function inside the getLocationName .then block
  useEffect(() => {
    if (locationName !== "") {
      const sendLocationToBackend = () => {
        if (latitude !== null && longitude !== null) {
          const locationData = {
            locationName: locationName,
            latitude: latitude,
            longitude: longitude,
          };
          console.log(locationData)
          const jsonData = JSON.stringify(locationData);
          console.log(jsonData)
          axios.post("http://127.0.0.1:8000/api/locations/", jsonData,{
            headers:{
              'Content-Type':'application/json',
            },
          })
            .then((response) => {
              console.log("Location data sent successfully:", response.jsonData);
            })
            .catch((error) => {
              console.error("Error sending location data:", error);
            });
        }
      };

      sendLocationToBackend();
    }
  }, [locationName, latitude, longitude]);

  return (
    <>
      <div id="myMap" style={{ height: "70vh", width: "100vh", marginTop: '5rem', marginLeft: "20rem" }}></div>
      <Footer />
    </>
  );
}

export default Osm;


