
import React, { useState, useEffect } from "react";
import axios from "axios";
import L from 'leaflet/dist/leaflet';
import icon from "leaflet/dist/images/marker-icon.png";
import iconShadow from "leaflet/dist/images/marker-shadow.png";
import './ghome.css';
import Footer from "../../components/Footer";
import LocationComponent from "../../components/LocationComponent";
import 'leaflet/dist/leaflet.css';
function Ghome(props) {
  const [latitude, setLatitude] = useState(null);
  const [longitude, setLongitude] = useState(null);
  const [destination, setDestination] = useState({ lat: "", lon: "" });
  const [route, setRoute] = useState(null);
  const DefaultIcon = L.icon({ iconUrl: icon, shadowUrl: iconShadow });
  const RedIcon = L.icon({ iconUrl: icon, shadowUrl: iconShadow });

  useEffect(() => {
    const getLocation = () => {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          setLatitude(position.coords.latitude);
          setLongitude(position.coords.longitude);
        },
        (error) => {
          console.warn("Error Code" + error.code + ": " + error.message);
          alert("Check the console");
        }
      );
    };

    getLocation();
  }, []);

  useEffect(() => {
    const fetchLocationData = async () => {
      try {
        const response = await axios.get('http://127.0.0.1:8000/api/get_location_data/'); 
         console.log('Hello',response.data)
        if (response.data) {
          const {locationName, latitude, longitude } = response.data;
          console.log(locationName,latitude,longitude)
          setDestination({ lat: latitude, lon: longitude });
        }
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };
    fetchLocationData();
  }, []);

  const handleFormSubmit = (e) => {
    e.preventDefault();

    const inputLatitude = destination.lat;
    const inputLongitude = destination.lon;

    if (!inputLatitude || !inputLongitude) {
      alert("Please enter both latitude and longitude values.");
      return;
    }

    const coordinates = { lat: inputLatitude, lon: inputLongitude };
    setDestination(coordinates);
    calculateRoute(coordinates);
  };

  const calculateRoute = (destinationCoordinates) => {
    if (latitude !== null && longitude !== null) {
      axios
        .get(
          `https://router.project-osrm.org/route/v1/driving/${longitude},${latitude};${destinationCoordinates.lon},${destinationCoordinates.lat}?steps=true&geometries=geojson`
        )
        .then((response) => {
          const routeGeoJSON = response.data.routes[0].geometry;
          setRoute(routeGeoJSON);
        })
        .catch((error) => {
          console.error("Error fetching route data:", error);
        });
    }
  };

  useEffect(() => {
    if (latitude !== null && longitude !== null) {
      const container = L.DomUtil.get("myMap");
      if (container != null) {
        container._leaflet_id = null;
      }

      const map = L.map("myMap").setView([latitude, longitude], 12);

      L.tileLayer(
        "https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}",
        {
          attribution:
            'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
          maxZoom: 20,
          id: "mapbox/streets-v11",
          tileSize: 512,
          zoomOffset: -1,
          accessToken:
            "pk.eyJ1IjoidGFyLWhlbCIsImEiOiJjbDJnYWRieGMwMTlrM2luenIzMzZwbGJ2In0.RQRMAJqClc4qoNwROT8Umg",
        }
      ).addTo(map);
      L.Marker.prototype.options.icon = DefaultIcon;

      if (typeof props.lan !== 'undefined' && typeof props.lon !== 'undefined') {
        const marker = L.marker([props.lan, props.lon]).addTo(map);
        marker.bindPopup("<b>destination</b>").openPopup();
      }

      const marker1 = L.marker([latitude, longitude]).addTo(map);
      marker1.bindPopup("<b>Your location</b>").openPopup();

      if (route !== null) {
        L.geoJSON(route).addTo(map);
      }

      if (destination.lat !== "" && destination.lon !== "") {
        const destinationMarker = L.marker([destination.lat, destination.lon], { icon: RedIcon }).addTo(map);
        destinationMarker.bindPopup("<b>Destination</b>").openPopup();
      }
      const yourLocationMarker = L.marker([latitude, longitude]).addTo(map);
      yourLocationMarker.bindPopup("<b>Your Location</b>").openPopup();
    }
  }, [latitude, longitude, props.lan, props.lon, route]);

  return (
    <>
    <LocationComponent/>
      <div>
        <form onSubmit={handleFormSubmit} className="ghomeform">
          <label className="ghomelabel"> Latitude: <input type="text" className="ghomeinput" name="latitude" value={destination.lat} onChange={(e) => setDestination({ ...destination, lat: e.target.value })} />
          </label>
          <br />
          <label className="ghomelabel"> Longitude: <input type="text" className="ghomeinput" name="longitude" value={destination.lon} onChange={(e) => setDestination({ ...destination, lon: e.target.value })} />
          </label>
          <br />
          <button type="submit" className="ghomebutton">Get Directions</button>
        </form>
      </div>

      <div id="myMap" style={{ height: "65vh", width: "90vh", marginTop: '-26rem', marginLeft: "4rem" }}></div>
      <Footer />
    </>
  );
}

export default Ghome;
