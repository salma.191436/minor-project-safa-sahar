
import React, { useEffect, useState } from 'react';
import './naghome.css';
import Footer from '../../components/Footer';
import axios from 'axios'; 
import {useNavigate} from 'react-router-dom'; 

const NagHome = () => {
  const [data, setData] = useState({
    totalWaste: 0,
    totalAmount: 0,
    totalDonation: 0,
  }); 

  const navigate =useNavigate();
  const handleSubmit =() =>{
    alert('Location Shared Successfull');
    navigate('/nagarik/osm');
  }
  useEffect(() => {
    axios.get('http://127.0.0.1:8000/api/get_contribution_data/') //backend api
      .then((response) => {
        console.log("API response",response.data)
        setData(response.data);
      })
      .catch((error) => {
        console.error('Error fetching data:', error);
      });
  }, []);

  return (
    <div>
      <div className='ngmain__container__box'>
        <h2 className='nghome__h2'>Nagarik DashBoard</h2>
        <ul className='nghome__ul'>
          <li className='nghome__li'>
            <h3 className='nghome__h3'>Total 3RWaste Dumped</h3>
            <h4 className='nghome__h4'>{data.totalWaste} KG</h4>
          </li>
          <li className='nghome__li'>
            <h3 className='nghome__h3'>Total Income</h3>
            <h4 className='nghome__h4'>Rs. {data.totalAmount}</h4>
          </li>
          <li className='nghome__li'>
            <h3 className='nghome__h3'>Total Donation</h3>
            <h4 className='nghome__h4'>Rs. {data.totalDonation}</h4>
          </li>
        </ul>
      </div>
      <button onClick={handleSubmit} className='ngbtn'>Send Request</button>
     
      <Footer />
    </div>
  );
};

export default NagHome;

