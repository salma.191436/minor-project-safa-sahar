import React, { useState } from 'react';
import axios from 'axios';
import './cform.css'
import Footer from '../../components/Footer';
import { useNavigate } from 'react-router-dom';

const Cform = () => {
  const navigate=useNavigate();
  const [wasteType, setWasteType] = useState('');
  const [totalWaste, setTotalWaste] = useState('');
  const [totalAmount, setTotalAmount] = useState('');
  const [totalDonation, setTotalDonation] = useState('');

  const handleSubmit = async (e) => {
    e.preventDefault();

    const numberPattern = /^[0-9]+(\.[0-9]*)?$/;
    const stringPattern = /^[A-Za-z\s]+$/;

    if (!stringPattern.test(wasteType.trim())) {
      alert('Waste type cannot be number');
      return;
    }
    if (!numberPattern.test(totalWaste) || !numberPattern.test(totalAmount) || !numberPattern.test(totalDonation)) {
      alert('Please enter valid numeric values for Total Waste, Total Amount, and Total Donation.');
      return;
    }

    const parsedTotalWaste = parseFloat(totalWaste);
    const parsedTotalAmount = parseFloat(totalAmount);
    const parsedTotalDonation = parseFloat(totalDonation);
   
    if (parsedTotalDonation > parsedTotalAmount) {
      alert('Donation amount cannot be higher than the Total Amount.');
      return;
    }
    let contribution_data={
      wasteType:wasteType.trim(),
      totalWaste: parsedTotalWaste,
      totalAmount: parsedTotalAmount,
      totalDonation: parsedTotalDonation,
    };
    console.log(contribution_data);
    console.log(JSON.stringify(contribution_data))
    fetch('http://127.0.0.1:8000/api/contribution/', { //contibution api point
      method: 'POST',
      headers: { 'content-type': 'application/json' },
      body: JSON.stringify(contribution_data),
  })
  .then((res) => {
    alert('Contribution form send Successfully');
    navigate('/nagarik/naghome');
  })
  .catch((err) => {
    alert('Failed: ' + err.message);
  });
}

  return (
    <>
    <form onSubmit={handleSubmit} className='cform'>
      <h2 className='ch2'>Contribution Form</h2>
      <div>
        <label className='clabel'>Type of waste:</label>
        <input className='cinput'  type="text"  value={wasteType}  onChange={e => setWasteType(e.target.value)}  required
        />
      </div>
      <div>
        <label className='clabel'>Total Waste:</label>
        <input className='cinput'  type="number"  value={totalWaste}  onChange={e => setTotalWaste(e.target.value)}  required
        />
      </div>
      <div>
        <label className='clabel'>Total amount:</label>
        <input className='cinput'  type="number"  value={totalAmount}  onChange={ e =>setTotalAmount(e.target.value)}  required />
      </div>
      <div>
        <label className='clabel'>Total donation:</label>
        <input
        className='cinput'  type="number" value={totalDonation}  onChange={ e =>setTotalDonation(e.target.value)}  required />
      </div>
      <button type="submit" className='cbutton'>Submit</button>
    </form>
    <Footer />
   </>
  );
};

export default Cform;
